import subprocess

# Originally from the mimod libraries


class bcfViewer (object):
    def __init__ (self, ifile, regions):
        self.ifile = ifile
        self.regions = regions
        call = []
        if regions:
            r =regions[0]+":"+regions[1]+"-" +regions[2]
            call = [u'/data/apps/software/bcftools-1.2/bcftools', u'view',
                    u'-r', r, ifile]
        else:
            call = [u'/data/apps/software/bcftools-1.2/bcftools', u'view', ifile]

        # call = [u'/data/apps/software/bcftools-1.2/bcftools', u'view', self.ifile]
        self.subprocess = subprocess.Popen(call,
                                           stdout = subprocess.PIPE,
                                           universal_newlines = True)
        self.stream = self.subprocess.stdout

    def __iter__ (self):
        return self

    def next (self):
        return self.stream.next()

    def readline (self):
        return self.stream.readline()

    def close (self):
        self.stream.close()

def view (ifile, regions):
    return bcfViewer(ifile, regions)
