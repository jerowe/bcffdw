# This is only available with 3.4 noooooo
# from MiModD import pyvcf, pybcftools, variant_calling
# Used 3to2 to convert pyvcf and pybcftools to 2.7
import pyvcf
from pprint import pprint
from logging import WARNING
import json
import re


class BcfFdw(object):

    """A foreign data wrapper for accessing bcf files.
    Valid options:
        - filename : full path to the csv file, which must be readable
        by the user running postgresql (usually postgres)
        """


    def __init__(self):
        self.t = 't'

 #%cpaste
    def genotypes(sam, GT):
        geno = {}
        for s in sam:
            tg = re.split(r'\||\\', GT[s])
            if int(tg[0]) == 1 and int(tg[1]) == 1:
                geno[s] = 'hom_alt'
            elif int(tg[0]) == 0 and int(tg[1]) == 1:
                geno[s] = 'het'
            elif int(tg[0]) == 0 and int(tg[1]) == 0:
                geno[s] = 'hom_ref'
            else:
                geno[s] = ''

        return json.dumps(geno)

    def execute(self):
        f = "/tmp/5samples.chr22.bcf"
        v = pyvcf.open(f, 'rb')

        #log_to_postgres("These are the quals", WARNING)
        #log_to_postgres(pprint(quals))

        #log_to_postgres(str(sorted(quals)))
        #log_to_postgres(str(sorted(columns)))

        sam = v.info.sample_names

        for l in v:
            r = {}
            r['contig'] = str(l.chrom)
            r['start'] = int(l.pos)
            r['end'] = int(l.pos)
            r['ref'] = str(l.ref)
            r['alt'] = str(l.alt)
            #r['filter'] = str(l.filter)
            r['info'] = json.dumps(l.info)
            r['sampleinfo'] = json.dumps(l.sampleinfo)
            r['samplegt'] = genotypes(sam, l.sampleinfo.get('GT'))
            #yield r
            print r


        v.close()


def view():
    return BcfFdw()
