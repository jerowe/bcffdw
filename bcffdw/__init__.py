from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres
from logging import WARNING
import json
import re
import operator
from collections import defaultdict
from bcffdw import pyvcf
# This is only available with 3.4 noooooo
# from MiModD import pyvcf, pybcftools, variant_calling
# Used 3to2 to convert pyvcf and pybcftools to 2.7


class BcfFdw(ForeignDataWrapper):

    """A foreign data wrapper for accessing bcf files.
        Valid options:
        - filename : full path to the bcf file, which must be readable
        by the user running postgresql (usually postgres)
    """
    def with_seq(self, quals, columns):
        # log_to_postgres("with seq")

        contig = []
        start = []
        end = []

        # Going to decide that we don't actually care about conditions for seq.
        # If there is a start/end we're going to assume it is start > X < end
        # 2.0 will include qualifiers for mapping/quality/whatever conditions

        clist = self.clauses.get('contig')
        for c in clist:
            for k, v in c.items():
                contig.append(k)

        clist = self.clauses.get('start')
        for c in clist:
            for k, v in c.items():
                start.append(k)

        clist = self.clauses.get('end')
        for c in clist:
            for k, v in c.items():
                end.append(k)

        for c in contig:
            c = str(c)
            for s in start:
                for e in end:
                    cols = [c, str(s), str(e)]
                    yield cols

    def no_seq(self, quals, columns):
        cols = []
        yield cols

    def repr_quals(self, quals, columns):
        self.clauses.clear()
        for qual in quals:
            # log_to_postgres("Qual is %s " % qual)
            operator = self.operators.get(qual.operator, None)
            if qual.field_name == 'contig':
                t = qual.value
                # t = t.replace('chr', '')
                qual.value = t
            if operator:
                self.clauses[qual.field_name].append({qual.value: operator})

        if self.clauses.get('contig') and self.clauses.get('start') and self.clauses.get('end'):
            # log_to_postgres("We have a contig, start, end!")
            return self.with_seq(quals, columns)
        elif self.clauses.get('contig') and self.clauses.get('end'):
            self.clauses['start'].append({1: self.operators.get('=')})
            # log_to_postgres("There is a contig and an end!")
            return self.with_seq(quals, columns)
        elif self.clauses.get('contig') and self.clauses.get('start'):
            clist = self.clauses.get('contig')
            for i in clist:
                # log_to_postgres("items are ")
                # log_to_postgres(i)
                for k,v in i.items():
                    # log_to_postgres("K is %s and V is %s " % (v, k))
                    self.clauses['end'].append({self.hg19cord.get(k) : self.operators.get('=')})
            return self.with_seq(quals, columns)
        elif self.clauses.get('contig'):
            self.clauses['start'].append({1: self.operators.get('=')})
            clist = self.clauses.get('contig')
            for i in clist:
                # log_to_postgres("items are ")
                # log_to_postgres(i)
                for k,v in i.items():
                    # log_to_postgres("K is %s and V is %s " % (v, k))
                    self.clauses['end'].append({self.hg19cord.get(k) : self.operators.get('=')})
            return self.with_seq(quals, columns)
        else:
            # log_to_postgres(" You did not supply a reference. "
                            # " Iterating through entire bed file. "
                            # " This will take awhile are you sure? ")
            return self.no_seq(quals, columns)

    def __init__(self, fdw_options, fdw_columns):
        super(BcfFdw, self).__init__(fdw_options, fdw_columns)
        self.filename = fdw_options["filename"]
        self.columns = fdw_columns
        self.operators = {
            '=': operator.eq,
            '<': operator.lt,
            '>': operator.gt,
            '<=': operator.le,
            '>=': operator.ge,
            '<>': operator.ne,
            '~~': re.match,
            '~~*': re.match,
            ('=', True): operator.truth,
            ('<>', False): operator.is_not
        }
        self.clauses = defaultdict(list)
        self.hg19cord = {
            '1':    249250621,
            '2':    243199373,
            '3':    198022430,
            '4':    191154276,
            '5':    180915260,
            '6':    171115067,
            '7':    159138663,
            'X':    155270560,
            '8':    146364022,
            '9':    141213431,
            '10':   135534747,
            '11':   135006516,
            '12':   133851895,
            '13':   115169878,
            '14':   107349540,
            '15':   102531392,
            '16':   90354753,
            '17':   81195210,
            '18':   78077248,
            '20':   63025520,
            'Y':   59373566,
            '19':   59128983,
            '22':   51304566,
            '21':   48129895
        }

    def genotypes(self, sam, GT):
        geno = {}
        for s in sam:
            tg = re.split(r'\||\\', GT[s])
            if int(tg[0]) == 1 and int(tg[1]) == 1:
                geno[s] = 'hom_alt'
            elif int(tg[0]) == 0 and int(tg[1]) == 1:
                geno[s] = 'het'
            elif int(tg[0]) == 0 and int(tg[1]) == 0:
                geno[s] = 'hom_ref'
            else:
                geno[s] = ''

        return json.dumps(geno)

    def execute(self, quals, columns):
        log_to_postgres("These are the quals", WARNING)

        log_to_postgres(str(sorted(quals)))
        log_to_postgres(str(sorted(columns)))

        for cols in self.repr_quals(quals, columns):
            log_to_postgres("Cols are ")
            log_to_postgres(cols)

            v = pyvcf.open(self.filename, cols, 'rb')
            sam = v.info.sample_names

            for l in v:
                r = {}
                r['contig'] = str(l.chrom)
                r['start'] = int(l.pos)
                r['end'] = int(l.pos)
                r['ref'] = str(l.ref)
                r['alt'] = str(l.alt)
                r['filter'] = str(l.filter)
                r['info'] = json.dumps(l.info)
                r['sampleinfo'] = json.dumps(l.sampleinfo)
                r['samplegt'] = self.genotypes(sam, l.sampleinfo.get('GT'))
                yield r

            v.close()
