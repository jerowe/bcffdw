CREATE SERVER bcf_srv foreign data wrapper multicorn options (
    wrapper 'bcffdw.BcfFdw'
    );

CREATE FOREIGN TABLE bcf5samples (
    "contig" VARCHAR(50),
    "start" INT,
    "end" INT,
    "ref" VARCHAR(200),
    "alt" VARCHAR(200),
    "filter" VARCHAR(200),
    "info" JSON,
    "sampleinfo" JSON, 
    "samplegt" JSON
) server bcf_srv options(
    filename '/data/coredata/advcomp/1Kbam/raw_data/bcf/5samples.chr22.bcf'
);

select * from bcf5samples limit 10;
