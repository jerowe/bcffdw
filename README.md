# NAME

BcfFdw

# DESCRIPTION

This is a wrapper around Bcftools using the Mimodd libraries and developed for Multicorn and postgres.

# ACKNOWLEDGEMENTS

This module was originally developed at and for Weill Cornell Medical College in Qatar. With approval from WCMC-Q, this information was generalized and put on github, for which the authors would like to express their gratitude.


