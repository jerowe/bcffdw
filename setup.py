import subprocess
from setuptools import setup, find_packages, Extension

setup(
    name='bcffdw',
    version='0.0.1',
    author='Jillian Rowe',
    license='Postgresql',
    packages=['bcffdw']
)
